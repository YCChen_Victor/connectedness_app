import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import os
import json
import pickle
from functions.dash_related import save_file, uploaded_files

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Connectedness Network"

# load Prerequisite
file_dir = os.path.dirname(os.path.abspath(__file__))
with open(file_dir + '/docs' + '/Prerequisite.json') as f:
    prerequisite = json.load(f)

# the app layout
app.layout = html.Div([
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),
    html.Div(
        id='output-data-upload'
        ),
    html.Button(
        'Summarize',
        id='button'
        ),
    # the place to display table
    html.H6(
        children='Connectedness Table'
        ),
    html.Div(
        id='table'
        )
])

# for files uploading
@app.callback(Output('output-data-upload', "children"),
              [Input("upload-data", "contents")],
              [State('upload-data', 'filename'),
               # State('upload-data', 'last_modified')
               ])
def update_output(list_of_contents, list_of_names):
    """Save uploaded files and regenerate the file list."""
    if list_of_contents is not None:
        for i in range(len(list_of_contents)):
            data = list_of_contents[i]
            filename = list_of_names[i]
            save_file(filename, data)
    files = uploaded_files()
    if len(files) == 0:
        return [html.Li("No files yet!")]
    else:
        return [html.Li(file) for file in files]

'''
# for calculate connectedness
@app.callback(
    Output('table', 'children'),
    [Input('button', 'n_clicks')]
)
def calculate_connectedness(n_click):

    if n_click != 0:
        import load_and_modify_data
        import volatility
        import connectedness_table

        file_path = os.path.dirname(os.path.realpath(__file__))
        save_path = file_path + '/docs/' + 'connectedness_table.pickle'
        with open(save_path, 'rb') as f:
            df = pickle.load(f)

        return html.Table(
                # Header
                [html.Tr([html.Th(col) for col in df.columns]) ] +
                # Body
                [html.Tr([
                    html.Td(df.iloc[i][col]) for col in df.columns
                ]) for i in range(len(df))]
            )
'''

if __name__ == '__main__':
    app.run_server(debug=True)