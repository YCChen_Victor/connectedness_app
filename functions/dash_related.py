import os
from urllib.parse import quote as urlquote
import dash_html_components as html
import base64

DIRECTORY = "./docs/files"

def save_file(name, content): # 先寫唯一一個檔案的，之後再寫多個檔案
    """Decode and store a file uploaded with Plotly Dash."""
    data = content.encode("utf8").split(b";base64,")[1]
    print(os.path.join(DIRECTORY, name))
    with open(os.path.join(DIRECTORY, name), "wb") as fp:
        fp.write(base64.decodebytes(data))

def uploaded_files():
    """List the files in the upload directory."""
    files = []
    for filename in os.listdir(DIRECTORY):
        path = os.path.join(DIRECTORY, filename)
        if os.path.isfile(path):
            files.append(filename)
    return files

def file_download_link(filename):
    """Create a Plotly Dash 'A' element that downloads a file from the app."""
    location = "/download/{}".format(urlquote(filename))
    return html.A(filename, href=location)

def summarized_files():
    """List the files in the summarized directory."""
    files = []
    for filename in os.listdir(DIRECTORY):
        path = os.path.join(DIRECTORY, filename)
        if os.path.isfile(path):
            files.append(filename)
    return files
